import axios from 'axios';
// import {USER_DETAIL, POST_DETAIL} from './actionType'
const URL = 'https://jsonplaceholder.typicode.com';



export function userDetails() {
  const request = axios.get(`${URL}/users`)
                  .then(res => res.data)
                  .catch((error)=>{
                    alert("There is an error in API call.");
                    });
  return {
    type:'USER_DETAIL.USER',
    payload:request
  }
}


export function postDetails(id) {
  const request = axios.get(`${URL}/users/${id}/posts`)
                  .then(res => res.data)
                  .catch((error)=>{
                    alert("There is an error in API call.");
                    });
  return {
    type:'POST_DETAIL.POST',
    payload:request
  }
}

//${URL}/posts/${id}/comments

export function commentDetails(id) {
  const request = axios.get(`${URL}/posts/${id}/comments`)
                  .then(res => res.data)
                  .catch((error)=>{
                    alert("There is an error in API call.");
                    });
  return {
    type:'COMMENT_DETAIL.COMMENT',
    payload:request
  }
}


// const fetchFailed = () => ({
//   type: USER_DETAIL.FETCH_FAILED
// })
// const fetchUserPageSuccess = (data) => ({
//   type: USER_DETAIL.FETCH_SUCCESS,
//   homePage: data,
// })
// export function fetchUserPage(){
//   return (dispatch) => {
//     axios.get(`${URL}/users`).then((res)=>{
//       if(res.status === 200){
//         dispatch(fetchUserPageSuccess(res.data))
//       }else{
//         dispatch(fetchFailed())
//       }
//     })
//   }
// }



// const fetchSuccessPostDetail = (dataPosts,dataComments) =>({
//   type: POST_DETAIL.FETCH_SUCCESS,
//   dataPost:dataPosts,
//   dataComments: dataComments,
// })
// const fetchFailedPostDetail = () =>({
//   type: POST_DETAIL.FETCH_FAILED
// })


// export function fetchPagePostDetailComment(id){
//   return(dispatch) => {
//     const getPostById = () =>  axios.get(`${URL}/users/${id}`);
//     const getCommentsById = () =>  axios.get(`${Config.apiUrl}/posts/${id}/comments`);
//     axios.all([getPostById(),getCommentsById()])
//       .then(axios.spread((dataPost,dataComments)=>(
//         dispatch(fetchSuccessPostDetail(dataPost,dataComments))
//       )))
//       .catch((err)=>{
//         dispatch(fetchFailedPostDetail());
//       })
//   }
// }



// const fetchSuccessUserPostDetails = (dataUser, dataPosts) => ({
//     type:'USER_POST_DETAILS',
//     dataUser: dataUser,
//     dataPosts: dataPosts
// })

// export function userPostDetails() {
//   return(dispatch) => {
//     const getUserById = () => axios.get(`${URL}/users/${id}`);
//     const getUserPostById = () => axios.get(`${URL}/users/${id}/posts`);

//     axios.all([getUserById(), getUserPostById()])
//                   .then(axios.spread((dataUser, dataPosts) => (
//                     dispatch(fetchSuccessUserPostDetails(dataUser, dataPosts))
//                   )))
//                   .catch((error) => {
//                     alert("There is an error in API call");
//                   });
//   }    
// }