import React, { Component } from 'react';
import '../userPresentation.css'
import 'bootstrap/dist/css/bootstrap.min.css'
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {commentDetails} from '../../actions';
import {bindActionCreators} from 'redux';
import PostCommentFromUser from '../postCommentFromUser/postCommentFromUser';
import Collapsible from 'react-collapsible';
import '../postCommentFromUser/postCommentFromUser.css';

class CommentPresentational extends Component {
    constructor() {
        super()
        // this.renderUser = this.renderUser.bind(this)
    }
    componentWillMount() {
        this.props.commentDetails(this.props.id);
    }
    // static getDerivedStateFromProps(nextPrps, prevState) {
    //     console.log( 'getDerivedStateFromProps test ',nextPrps, prevState)
    // }
    // getSnapshotBeforeUpdate() {
    //     console.log('Inside getSnapshotBefore Update')
    // }
 
    render() {
    //    console.log(this.props)
        const renderUser = (users) => {
            // return users && users.length > 0 ?
                // users.map((user, index) => {
                //     // return (this.props.id === user.postId) ?
                           
                //             // : null
                // }):null
                return ( users && users.length > 0 ?
                    // users.filter((data) => {
                    //     return data.postId === this.props.id
                    // })
                    users.map((user, index) => {
                        return <tr key={index}>
                                    {/* <Link to={`/posts/${user.id}/comments`} key={user.id}> */}
                                    <td>{user.id}</td>
                                    <td>{user.postId}</td>
                                    <td>{user.name}</td>
                                    <td>{user.email}</td>
                                    <td>{user.body}</td>
                                </tr>
                }):null)
        }
        const rendering = (users) => {
            return users && users.length > 0 ?
                users.map((user, index) => {
                    
                    return(
                        
                        <div key={index}>
                            <div className="collapseBox">
                                <Collapsible trigger={<h5>{user.id}. {user.name}</h5>}>
                                    <div className="collapsingData">
                                        <p>{user.body}</p>
                                        {/* <div>
                                            <Collapsible trigger={ <div className="commentBtn">
                                                    <Link to={{pathname:`/posts/${user.id}/comments`,state: { userId: user.id }}} key={user.id}>
                                                        <button onClick={this._onButtonClick}>View Comments</button>
                                                    </Link>

                                                </div>
                                                }>
                                                <hr></hr>
                                                
                                                <div className="collapsingComment">
                                                {this.state.showComponent ?
                                                    <CommentPresentational  id={proping.location.state.userId}/> :null
                                                }
                                                    <p>This is the collapsible content. It can be any element or React component you like.
                                                        It can even be another Collapsible component. Check out the next section!This is the
                                                        collapsible content. It can be any element or React component you like.It can even be another 
                                                        Collapsible component. Check out the next section!
                                                    </p>
                                                </div>
                                            </Collapsible>
                                        </div> */}
                                    </div>
                                </Collapsible>
                            </div>
                            <hr></hr>
                        </div>)
                }):null
        }
        // let imgStyle= {
        //     width: '300px'
        // }
       

        return(
            <div>
                <div className="userDetails">
                    <hr></hr><h1>User Post Comments Details</h1><hr></hr>
                    {/* <img src="https://media1.giphy.com/media/xTiTnETzih3HYCaw2Q/giphy.gif?cid=3640f6095c1b70746e6b376332b812c3" style={imgStyle}></img> */}
                </div>
                <div className="container">
                <h2>{this.props.id}</h2>
                {rendering(this.props.data)}
                    <table className="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th><b>Id</b></th>
                                <th>PostID</th>
                                <th>Name</th>
                                <th>EmailID</th>
                                <th>Comments</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            {renderUser(this.props.data)}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    // console.log("data : ",state)
    return {
        data: state.users ? state.users.commentReducer : []
        // data: state.users.commentReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        commentDetails
    },dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentPresentational)