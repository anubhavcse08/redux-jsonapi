import React from 'react'
import {shallow, mount } from '../../../../enzyme'
import configureStore from 'redux-mock-store';
import UserHomeContainer from '../../../../containers/userHomeContainer';
import { shallowToJson } from 'enzyme-to-json';

describe('UserHomeContainer Test', () => {
    let initState ={};
    let mockStore = configureStore();
    let wrapper, store;

    beforeEach(() => {
        store = mockStore(initState);
        wrapper = shallow(<UserHomeContainer store={store}/>)
    });
    afterEach(() => {
        wrapper.unmount();
    })
    it('Should render correctly', () => {
        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });
    it('UserHomeContainer items', () => {
        expect(wrapper.find('div')).toBeDefined();
    });
    // it('allows us to set props', () => {
    //     const wrapper = mount(
    //       <UserHomeContainer
    //         store={store}
    //         checking={'check'}
    //         data ={[]}
    //       />
    //     );
    //     expect(wrapper.props().checking).toEqual('check')
    // });
    it('should show previously rolled value', () => {
        // test that the state values were correctly passed as props
        expect(wrapper.props().data).toEqual([]);
    });
})