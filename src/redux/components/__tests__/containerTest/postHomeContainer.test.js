import React from 'react'
import {shallow, mount } from '../../../../enzyme'
import configureStore from 'redux-mock-store';
import { shallowToJson } from 'enzyme-to-json';
import PostHomeContainer from '../../../../containers/postHomeContainer';

describe('PostHomeContainer Test', () => {
    let initState ={};
    let mockStore = configureStore();
    let wrapper, store;

    beforeEach(() => {
        store = mockStore(initState);
        wrapper = shallow(<PostHomeContainer store={store}/>)
    });
    afterEach(() => {
        wrapper.unmount();
    })
    it('Should render correctly', () => {
        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });
    it('PostHomeContainer items', () => {
        expect(wrapper.find('div')).toBeDefined();
    });
    it('allows us to set props', () => {
        const wrapper = mount(
          <PostHomeContainer
            store={store}
            checking={'check'}
            data ={[]}
          />
        );
        expect(wrapper.props().checking).toEqual('check')
    });

    it('should show previously rolled value', () => {
        // test that the state values were correctly passed as props
        expect(wrapper.props().data).toEqual([]);
    });
    it('should events dispatch the expected actions', () => {
        wrapper.simulate('POST_DETAIL.POST');
        const action = store.getActions();
        expect(action).toEqual([]);
    });
})