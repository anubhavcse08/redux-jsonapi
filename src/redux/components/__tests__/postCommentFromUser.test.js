import React from 'react'
import {shallow, mount } from '../../../enzyme'
import PostCommentFromUser from '../postCommentFromUser/postCommentFromUser';
import { shallowToJson } from 'enzyme-to-json';

describe('PostCommentFromUser Test', () => {

    let output = shallow(
        <PostCommentFromUser
            fromPostPresent ={{ data: [], location: { state: { userName : ''}}}}
        />
    );
    let wrapper = mount(
        <PostCommentFromUser
            checking={'check'}
            fromPostPresent ={{ data: [], location: { state: { userName : ''}}}}
        />
    );

    it('Should render correctly', () => {
        expect(shallowToJson(output)).toMatchSnapshot();
    });
    it('should render component onetime', () => {
        expect(output.length).toEqual(1);
    });
    it('allows us to set props', () => {
        expect(wrapper.props().checking).toEqual('check')
    });
    it('ShowComponent State is there', () => {
        expect(wrapper.state.showComponent).toBe();
    });
    it('Html attributes Testing', () => {
        expect(output.find('div.container')).toBeDefined();
        expect(output.find('div.container.userName')).toBeDefined();
    });
    // it('on _onButtonClick', () => {
    //     wrapper.instance().state.showComponent = false;
    //     wrapper.instance()._onButtonClick();
    //     expect(wrapper.state.showComponent).toEqual(false);
    // });

});