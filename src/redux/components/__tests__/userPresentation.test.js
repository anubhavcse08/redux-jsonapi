import React from 'react';
import { shallow, mount } from '../../../enzyme';
import UserPresentational from '../userPresentation';
import { shallowToJson } from 'enzyme-to-json';

describe('UserPresentation Test', () => {
  let res;

  beforeEach(() => {
    res = shallow(
      <UserPresentational
        data={[]}
      />
    );
  });
  afterEach(() => {
    res.unmount();
})
  it('shound render correctly', () => {
    expect(shallowToJson(res)).toMatchSnapshot();
  })
  it('should render component onetime', () => {
    expect(res.length).toEqual(1);
  })
  it('allows us to set props', () => {
    const wrapper = mount(
      <UserPresentational
        checking={'check'}
        data ={[]}
      />
    );
    expect(wrapper.props().checking).toEqual('check')
  })


  it('render Table items', () => {
    const items = ['John', 'James', 'Luke'];
    const wrapper = shallow(<UserPresentational items={items} />);

    expect(wrapper.find('div.userDetails')).toBeDefined();
    expect(wrapper.find('div.container')).toBeDefined();
    expect(wrapper.find('thead').childAt(0).type()).toEqual('tr');
    expect(wrapper.find('thead').children()).toHaveLength(1);
    expect(wrapper.contains('<tbody>{renderUser(proping)}</tbody>'));
    // expect(wrapper.find('th').text()).toEqual('Name');
    // expect(wrapper.equals('<th>Name</th>')).toEqual(true);
    // expect(wrapper.find())
  })
});
