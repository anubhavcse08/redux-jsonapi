import React from 'react'
import {shallow, mount} from '../../../enzyme'
import PostPresentational from '../postPresnt/postPresentation';
import { shallowToJson } from 'enzyme-to-json';

describe('PostPresentation test', () => {
    let res;
    beforeEach(() => {
        res = shallow(
            <PostPresentational
                users={[]}
            />
        )
    });
    it('Should render correctly', () => {
        expect(shallowToJson(res)).toMatchSnapshot();
    });
    it('should render component onetime', () => {
        expect(res.length).toEqual(1);
    });
    // it('allows us to set props', () => {
    //     const wrapper = mount(
    //         <PostPresentational
    //         checking={'check'}
    //         fromPostPresent ={{ data: [], location: { state: { userName : ''}}}}
    //         />
    //     );
    //     expect(wrapper.props().checking).toEqual('check')
    // })
})