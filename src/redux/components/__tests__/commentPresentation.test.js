import React from 'react'
import { shallow, mount } from '../../../enzyme'
import  CommentPresentation from '../commentPresent/commentPresentation';
import { shallowToJson } from 'enzyme-to-json';
import configureStore from 'redux-mock-store';

describe('CommentPresentation Test ', () => {
    const initstate = {};
    const mockStore = configureStore();
    let wrapper;
    let store;
    beforeEach(() => {
        store = mockStore(initstate)
        wrapper = shallow( <CommentPresentation store={store}/>)
    })
    afterEach(() => {
        wrapper.unmount();
    })
    it('Should render correctly', () => {
        expect(shallowToJson(wrapper)).toMatchSnapshot();
    });
    it('Should function calling', () => {
        const mockFetch = jest.fn();
        let result = mockFetch('renderUser')
        expect(result).toBeUndefined();
        expect(mockFetch).toHaveBeenCalled();
        expect(mockFetch).toHaveBeenCalledTimes(1);
        expect(mockFetch).toHaveBeenCalledWith('renderUser')
    });
    it('Comment presentation items', () => {
        expect(wrapper.find('div.collapseBox')).toBeDefined();
    });
    it('allows us to set props', () => {
        const wrapper = mount(
          <CommentPresentation
            store={store}
            checking={'check'}
            data ={[]}
          />
        );
        expect(wrapper.props().checking).toEqual('check')
      })
});