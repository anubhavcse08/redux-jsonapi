import React from 'react';
// import CommentHomeContainer from '../../containers/commentHomeContainer';
import CommentPresentation from '../components/commentPresent/commentPresentation';

const CommentHome = (props) => {
    return (
        <div>
            <CommentPresentation {...props} />
        </div>
    )
}

export default CommentHome