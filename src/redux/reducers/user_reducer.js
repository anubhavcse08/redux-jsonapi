// import {USER_DETAIL, POST_DETAIL} from '../actions/actionType';
// const defaultStatePostDetails = {
//     dataUser:{},
//     dataPosts:[]
//   }


export default function(state = {}, action){
    switch(action.type){
        case 'USER_DETAIL.USER':
            return {...state,userReducer:action.payload}
        case 'POST_DETAIL.POST':
            return {...state,postReducer:action.payload}
        case 'COMMENT_DETAIL.COMMENT':
            return {...state,commentReducer:action.payload}
        // case 'USER_POST_DETAILS':
        //     return {
        //         ...state,
        //         dataUser:action.dataUser.data,
        //         dataPosts:action.dataPosts.data
        //     }
        default:
            return state;
    }
}